﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchZone : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
		print(collision);

		if (collision.gameObject.GetComponent<PlayerHandler>() != null)
        {
            PlayerHandler player = collision.gameObject.GetComponent<PlayerHandler>();
            print(player);
            player.canPlaceTorch = true;
            player.spawnPosition = transform.position;
        }
    }

	private void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.GetComponent<PlayerHandler>() != null) {
			PlayerHandler player = collision.gameObject.GetComponent<PlayerHandler>();
			player.canPlaceTorch = false;
		}
	}
}
