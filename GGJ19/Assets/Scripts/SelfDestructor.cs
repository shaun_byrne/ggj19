﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructor : MonoBehaviour
{
    public float destoryAfter;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destoryAfter);
    }
}
