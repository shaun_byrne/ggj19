﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb;
    public float horizontalMovement;
    public float verticalMovement;
    public float xSize;

    public enum Facing { right, left, up, down };
    public Facing facing = Facing.up;

    private int xDir = 0;
    private float footstepTimer = 0f;

    Animator animator;

    public float minX, minY, maxX, maxY;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        OnlyMoveBetweenX(minX, maxX);
        OnlyMoveBetweenY(minY, maxY);
        MovementInput();
        if(horizontalMovement > 0)
        {
            facing = Facing.right;
            xDir = 1;
        }
        if(horizontalMovement < 0)
        {
            facing = Facing.left;
            xDir = -1;
        }
        if(verticalMovement > 0)
        {
            facing = Facing.up;
        }
        if(verticalMovement < 0)
        {
            facing = Facing.down;
        }
        
        if(xDir > 0)
        {
            transform.localScale = new Vector2(xSize, transform.localScale.y);
        }
        else if(xDir < 0)
        {
            transform.localScale = new Vector2(-xSize, transform.localScale.y);
        }
    }

    void OnlyMoveBetweenX(float minX, float maxX)
    {
        if (transform.position.x >= maxX)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }

        if (transform.position.x < minX)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
    }
    void OnlyMoveBetweenY(float minY, float maxY)
    {
        if (transform.position.y >= maxY)
        {
            transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        }

        if (transform.position.y < minY)
        {
            transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        }
    }

    void MovementInput()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");

        verticalMovement = Input.GetAxisRaw("Vertical");

        rb.velocity = new Vector2(horizontalMovement, verticalMovement).normalized * moveSpeed * Time.deltaTime;

        animator.SetBool("IsRunning", (horizontalMovement != 0 ? true : false)|| 
            (verticalMovement != 0 ? true : false));

        if (rb.velocity.magnitude > 0.01f)
        {
            if (footstepTimer <= 0f)
            {
                GameManager.current.audioManager.PlayAudio("footStepSand");
                footstepTimer = 0.3f;
            }
            else
            {
                footstepTimer -= Time.deltaTime;
            }
        }
    }
}
