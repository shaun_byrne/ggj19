﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeController : MonoBehaviour
{
    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void CamShake()
    {
        animator.SetTrigger("CamShake");
    }

    public void CamBigShake()
    {
        animator.SetTrigger("CamBigShake");
    }
}
