﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour {
	public Texture enemyIcon;
	public Texture playerIcon;
	public Texture homeIcon;
	public float iconScale;
	public Image minimapBorder;
	public float zoom = 50;

	private GameObject player;
	private GameObject home;

	void Start() {
		player = GameObject.FindGameObjectWithTag("Player");
		home = FindObjectOfType<HomeHandler>().gameObject;
	}

	void OnGUI() {
		Rect rect = RectTransformToScreenSpace(minimapBorder.rectTransform);
		float size = rect.height;
		float scale = size / Camera.main.pixelHeight;

		//GUI.color = new Color(0, 0, 0, 0.1f);
		//GUI.DrawTexture(new Rect(rect.x - size / 2f, Camera.main.pixelHeight - rect.y - size / 2f, size, size), enemyIcon);
		//GUI.color = Color.white;

		Vector2 screenPos = (Vector2)home.transform.position * zoom;
		float x = Camera.main.pixelWidth - size / 2f + screenPos.x * size - iconScale / 2f;
		float y = size / 2f - screenPos.y * size - iconScale / 2f;

		if (x >= Camera.main.pixelWidth - size && y <= size) {
			GUI.DrawTexture(new Rect(x, y, iconScale, iconScale), homeIcon);
		}

		screenPos = (Vector2)player.transform.position * zoom;
		x = Camera.main.pixelWidth - size / 2f + screenPos.x * size - iconScale / 2f;
		y = size / 2f - screenPos.y * size - iconScale / 2f;

		if (x >= Camera.main.pixelWidth - size && y <= size) {
			GUI.DrawTexture(new Rect(x, y, iconScale, iconScale), playerIcon);
		}

		/*foreach (GameObject enemy in GameManager.current.enemies) {
			Vector2 screenPos = (Vector2)enemy.transform.position * zoom;
			float x = Camera.main.pixelWidth - size / 2f + screenPos.x * size - 1;
			float y = size / 2f - screenPos.y * size - 1;

			if (x >= Camera.main.pixelWidth - size && y <= size) {
				GUI.DrawTexture(new Rect(x, y, 2, 2), enemyIcon);
			}
		}*/
	}

	public static Rect RectTransformToScreenSpace(RectTransform t) {
		Vector2 size = Vector2.Scale(t.rect.size, t.lossyScale);
		return new Rect((Vector2)t.position - (size * 0.5f), size);
	}
}
