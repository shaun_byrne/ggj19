﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    PlayerAttack attack;
    // Start is called before the first frame update
    void Start()
    {
        attack = GetComponentInParent<PlayerAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AttackOnHit()
    {
        attack.Attack();
    }
}
