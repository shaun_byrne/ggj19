﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
	public GameObject linkedPortal;

	private GameObject player;

	void Start() {
		player = FindObjectOfType<PlayerHandler>().gameObject;
        linkedPortal = GameObject.FindGameObjectWithTag("LinkedPortal");
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.F) 
			&& !GameManager.current.portalCooldown
			&& Vector3.Distance(player.transform.position, transform.position) <= 1f) {
			GameManager.current.portalCooldown = true;
			player.transform.position = linkedPortal.transform.position;
            GameManager.current.audioManager.PlayAudio("teleport");
        }

		if (Input.GetKeyUp(KeyCode.F)) {
			GameManager.current.portalCooldown = false;
		}
	}
}
