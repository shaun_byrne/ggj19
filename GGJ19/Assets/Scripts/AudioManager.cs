﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {
	public AudioSource drumLoop;
	public AudioSource ambientLoop;
	public AudioClip introClip;
	public AudioClip loopClip;

    public AudioSource player;
    public AudioSource enemy;
    public AudioSource game;

    public AudioClip[] meleeHit;
    public AudioClip[] meleeMiss;
    public AudioClip[] shifterHit;

    public AudioClip gameOver;
    public AudioClip[] footStepSand;
    public AudioClip artifactDrop;
    public AudioClip footStepRock;
    public AudioClip teleport;

    public float drumBpm = 120f;
	private float drumTimer = 0f;
	private bool doPlayDrumLoop = false;

	void Start() {
		DontDestroyOnLoad(this.gameObject);

		//drumLoop.Play();
        
		ambientLoop.clip = introClip;
		ambientLoop.loop = false;
		ambientLoop.Play();

        
	}

	void Update() {
		/*if (doPlayDrumLoop) {
			doPlayDrumLoop = false;
			drumLoop.volume = 1f;
			drumLoop.Play();
		}

		if (drumTimer <= 0f) {
			drumTimer += (60f / drumBpm) * (Random.value > 0.5f ? 0.5f : 1f);

			drumLoop.volume = 0.1f;
			doPlayDrumLoop = true;
		} else {
			drumTimer -= Time.deltaTime;
		}*/

		if (!ambientLoop.isPlaying) {
            ambientLoop.clip = loopClip;
			ambientLoop.loop = true;
			ambientLoop.Play();
		}
	}

    public void PlayAudio(string name)
    {
        switch (name)
        {
            case "attack":
                player.volume = 1f;
                player.clip = meleeMiss[Random.Range(0, meleeMiss.Length)];
                player.Play();
                break;
            case "hit":
                player.volume = 1f;
                player.clip = meleeHit[Random.Range(0, meleeHit.Length)];
                player.Play();
                break;

            case "gameOver":
                player.volume = 1f;
                player.clip = gameOver;
                player.Play();
                break;

            case "footStepSand":
                player.volume = 0.25f;
                player.clip = footStepSand[Random.Range(0, 3)];
                player.Play();
                break;

            case "footStepRock":
                player.volume = 0.25f;
                player.clip = footStepRock;
                player.Play();
                break;

            case "artifactDrop":
                player.volume = 1f;
                player.clip = artifactDrop;
                player.Play();
                break;

            case "teleport":
                player.volume = 1f;
                player.clip = teleport;
                player.Play();
                break;
        }
    }
}
