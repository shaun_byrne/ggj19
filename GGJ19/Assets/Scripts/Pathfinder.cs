﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinder : MonoBehaviour {
	GameObject[] waypoints;
	List<PathNode> frontier;
	List<PathNode> explored;

	public void UpdateWaypoints() {
		waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
		//print("there are " + waypoints.Length + " waypoints");
	}

	public void ResetWaypoints() {
		waypoints = null;
	}

	public GameObject NextWaypoint(GameObject current, GameObject target) {
		if (current == null || target == null) {
			return null;
		}

		frontier = new List<PathNode>();
		explored = new List<PathNode>();

		frontier.Add(new PathNode(current, null, target, waypoints, 0));

		while (frontier.Count > 0) {
			PathNode node = frontier[0];
			frontier.RemoveAt(0);
			explored.Add(node);

			if (node.GetWaypoint() == target) {
				PathNode next = node.NextWaypoint();

				if (next != null) {
					return next.GetWaypoint();
				}
			} else {
				foreach (PathNode n in node.FindVisible()) {
					if (!IsExplored(n)) {
						AddToFrontier(n);

						if (frontier.Count > waypoints.Length) {
							return null;
						}
					}
				}
			}
		}

		return null;
	}

	public GameObject NearestWaypoint(Vector3 pos) {
		GameObject nearest = null;
		float nearestDist = Mathf.Infinity;

		foreach (GameObject w in waypoints) {
			int layermask = 1 << 12;
			Vector3 dir = w.transform.position - pos;
			RaycastHit2D hit = Physics2D.CircleCast(pos, GameManager.current.characterRadius, dir, dir.magnitude, layermask);
			//RaycastHit2D hit = Physics2D.Linecast(pos, w.transform.position, layermask);

			if (hit.collider == null) {
				float dist = Mathf.Abs(w.transform.position.x - pos.x) + Mathf.Abs(w.transform.position.y - pos.y);

				if (nearest == null || dist < nearestDist) {
					nearest = w;
					nearestDist = dist;
				}
			}
		}

		return nearest;
	}

	public GameObject NearestWaypointToTarget(Vector3 pos, Vector3 targetPos) {
		GameObject nearest = null;
		float nearestDist = Mathf.Infinity;

		foreach (GameObject w in waypoints) {
			int layermask = 1 << 12;
			Vector3 dir = w.transform.position - pos;
			RaycastHit2D hit = Physics2D.CircleCast(pos, GameManager.current.characterRadius, dir, dir.magnitude, layermask);
			//RaycastHit2D hit = Physics2D.Linecast(pos, w.transform.position, layermask);

			if (hit.collider == null) {
				float dist = Vector2.Distance(w.transform.position, targetPos);//Mathf.Abs(w.transform.position.x - targetPos.x) + Mathf.Abs(w.transform.position.y - targetPos.y);

				if (nearest == null || dist < nearestDist) {
					nearest = w;
					nearestDist = dist;
				}
			}
		}

		return nearest;
	}

	private bool IsExplored(PathNode node) {
		foreach (PathNode n in frontier) {
			if (node.GetWaypoint() == n.GetWaypoint()) {
				return true;
			}
		}

		foreach (PathNode n in explored) {
			if (node.GetWaypoint() == n.GetWaypoint()) {
				return true;
			}
		}

		return false;
	}

	private void AddToFrontier(PathNode node) {
		if (frontier.Count == 0) {
			frontier.Add(node);
		} else {
			bool success = false;

			for (int i = 0; i < frontier.Count; i++) {
				PathNode fNode = frontier[i];

				if (node.GetF() < fNode.GetF()) {
					frontier.RemoveAt(i);
					frontier.Insert(i, node);
					frontier.Insert(i + 1, fNode);
					success = true;
					i = frontier.Count;
				}
			}

			if (!success) {
				frontier.Insert(frontier.Count, node);
			}
		}
	}
}

public class PathNode {
	GameObject waypoint = null;
	GameObject target = null;
	GameObject[] waypoints = null;
	float f, g, h;
	PathNode parent = null;

	public PathNode(GameObject waypoint, PathNode parent, GameObject target, GameObject[] waypoints, float g) {
		this.waypoint = waypoint;
		this.parent = parent;
		this.target = target;
		this.waypoints = waypoints;
		this.g = g;
		//this.h = Vector3.Distance(waypoint.transform.position, target.transform.position);
		this.h = Mathf.Abs(target.transform.position.x - waypoint.transform.position.x) + Mathf.Abs(target.transform.position.y - waypoint.transform.position.y);
		this.f = g + h;
	}

	public GameObject GetWaypoint() {
		return waypoint;
	}

	public float GetF() {
		return f;
	}

	public PathNode NextWaypoint() {
		if (parent == null) {
			return null;
		} else {
			PathNode node = parent.NextWaypoint();

			if (node == null) {
				return this;
			} else {
				return node;
			}
		}
	}

	public PathNode[] FindVisible() {
		Waypoint wp = waypoint.GetComponent<Waypoint>();

		if (wp != null) {
			PathNode[] pathNodes = wp.GetPathNodes();

			if (pathNodes != null) {
				//return pathNodes;
			}
		}

		List<PathNode> visible = new List<PathNode>();

		foreach (GameObject w in waypoints) {
			int layermask = 1 << 12;
			Vector3 dir = w.transform.position - waypoint.transform.position;
			RaycastHit2D hit = Physics2D.CircleCast(waypoint.transform.position, GameManager.current.characterRadius, dir, dir.magnitude, layermask);
			//RaycastHit2D hit = Physics2D.Linecast(waypoint.transform.position, w.transform.position, layermask);

			if (hit.collider == null) {
				float dist = Mathf.Abs(w.transform.position.x - waypoint.transform.position.x) + Mathf.Abs(w.transform.position.y - waypoint.transform.position.y);
				visible.Add(new PathNode(w, this, target, waypoints, g + dist));
			}
		}

		if (wp != null) {
			wp.SetPathNodes(visible.ToArray());
			//return wp.GetPathNodes();
		}

		return visible.ToArray();
	}
}