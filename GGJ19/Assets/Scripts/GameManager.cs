﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static GameManager current;

    public AudioManager audioManager;
    public float characterRadius = 0.5f;
	public GameObject enemyPrefab;
	public float initialSpawnTimer = 1f;
	public float spawnRate = 5f;
	public float spawnRateIncrease = 0.1f;
	public float minSpawnRate = 1f;
	public float concealmentRadius = 5f;

	[System.NonSerialized]
	public Pathfinder pathfinder;
	[System.NonSerialized]
	public GameObject[] artifacts;
	[System.NonSerialized]
	public GameObject[] spawnPoints;
	[System.NonSerialized]
	public GameObject[] enemies;
	[System.NonSerialized]
	public GameObject[] concealments;
	[System.NonSerialized]
	public bool portalCooldown = false;

    private GameObject player;
	private float spawnTimer = 0f;
    public bool stopSpawn;

	void Start() {
		GameManager.current = this;
		pathfinder = GetComponent<Pathfinder>();
		pathfinder.UpdateWaypoints();
		UpdateArtifacts();
		player = GameObject.FindGameObjectWithTag("Player");
		spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
		concealments = GameObject.FindGameObjectsWithTag("Concealment");
		UpdateEnemies();
		spawnTimer = initialSpawnTimer;
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        ResetTime();
        if (stopSpawn == false)
        {
            if (spawnTimer <= 0f)
            {
                spawnTimer = spawnRate;
                spawnRate -= spawnRateIncrease * spawnRate;
                spawnRate = Mathf.Clamp(spawnRate, minSpawnRate, 10000);

				Vector3 spawnPos = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;

				while (Vector2.Distance(player.transform.position, spawnPos) <= 15f) {
					spawnPos = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
				}

				Instantiate(enemyPrefab, spawnPos, Quaternion.identity);
                UpdateEnemies();
            }
            else
            {
                spawnTimer -= Time.deltaTime;
            }
        }
    }

    void ResetTime()
    {
        if (Time.timeScale < 1)
        {
            Time.timeScale += 10 * Time.deltaTime;
        }
        else if (Time.timeScale > 1)
        {
            Time.timeScale = 1;
        }
    }

    public GameObject NearestArtifact(Vector3 pos) {
		GameObject nearest = null;
		float nearestDist = Mathf.Infinity;

		foreach (GameObject w in artifacts) {
			float dist = Mathf.Abs(w.transform.position.x - pos.x) + Mathf.Abs(w.transform.position.y - pos.y);

			if (nearest == null || dist < nearestDist) {
				nearest = w;
				nearestDist = dist;
			}
		}
		return nearest;
	}

	public GameObject NearestSpawnPoint(Vector3 pos) {
		GameObject nearest = null;
		float nearestDist = Mathf.Infinity;

		foreach (GameObject w in spawnPoints) {
			float dist = Mathf.Abs(w.transform.position.x - pos.x) + Mathf.Abs(w.transform.position.y - pos.y);

			if (nearest == null || dist < nearestDist) {
				nearest = w;
				nearestDist = dist;
			}
		}
		return nearest;
	}

	public GameObject RandomConcealment(Vector3 pos) {
		List<GameObject> closeObjects = new List<GameObject>();
		
		foreach (GameObject c in concealments) {
			float dist = Mathf.Abs(c.transform.position.x - pos.x) + Mathf.Abs(c.transform.position.y - pos.y);

			if (dist <= concealmentRadius) {
				closeObjects.Add(c);
			}
		}

		return closeObjects[Random.Range(0, closeObjects.Count)];
	}

	public void UpdateEnemies() {
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
	}

	public void UpdateArtifacts() {
		artifacts = GameObject.FindGameObjectsWithTag("Artifact");
	}

    public void NewLevel(int artifactCount)
    {
        if (artifactCount >= 10)
        {
            initialSpawnTimer = 1f;
            spawnRate = 7f;
            spawnRateIncrease = 0.005f;
            minSpawnRate = 1.5f;
        }
        else if (artifactCount >= 8)
        {
            initialSpawnTimer = 1f;
            spawnRate = 9f;
            spawnRateIncrease = 0.005f;
            minSpawnRate = 2f;
        }
        else
        {
            initialSpawnTimer = 1f;
            spawnRate = 12f;
            spawnRateIncrease = 0.005f;
            minSpawnRate = 2.5f;
}    }
}

