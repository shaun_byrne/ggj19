﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform hitOrigin;
    public float boxSize;
    PlayerMovement playerMovement;
    public Vector2 upHitPoint;
    public Vector2 downHitPoint;
    public Vector2 leftHitPoint;
    public Vector2 rightHitPoint;

    public float attackCoolDownTime;
    public float attackTime;
    public bool canAttack = true;

	private Vector2 originalOriginPos;

    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        //hitOrigin = transform;
        playerMovement = GetComponent<PlayerMovement>();
		originalOriginPos = hitOrigin.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.J) || Input.GetButtonDown("Fire2")) && canAttack == true)
        {
            GameManager.current.audioManager.PlayAudio("attack");
            animator.SetTrigger("attack");
            //ApplyDamage(1);
            StartCoroutine(waitForAttack());
        }
        AdjustHitBoxBasedOnFaceDir();
    }

    public void ApplyDamage(int attackRange)
    {
        Collider2D[] colliders = 
            Physics2D.OverlapBoxAll(
                hitOrigin.position, 
                new Vector2(boxSize, boxSize) * attackRange, 
                0, 
                LayerMask.GetMask("Enemy"));

        foreach(Collider2D collider in colliders)
        {
            if(collider.GetComponent<Enemy>() != null)
            {
                Enemy enemy = collider.GetComponent<Enemy>();
                enemy.TakeDamage();
            }
        }
    }

    public void Attack()
    {
        ApplyDamage(1);
    }

    void AdjustHitBoxBasedOnFaceDir()
    {
        bool useAlt = false;

        if (useAlt)
        {
            if (playerMovement.rb.velocity.normalized.magnitude != 0)
            {
                hitOrigin.position = (Vector2)transform.position + playerMovement.rb.velocity.normalized * 0.5f;
            }    
        }
        else
        {
            if(playerMovement.facing == PlayerMovement.Facing.up)
            {
                hitOrigin.localPosition = originalOriginPos + upHitPoint;
            }
            else if (playerMovement.facing == PlayerMovement.Facing.down)
            {
                hitOrigin.localPosition = originalOriginPos + downHitPoint;
            }
            else if (playerMovement.facing == PlayerMovement.Facing.right)
            {
                hitOrigin.localPosition = originalOriginPos + rightHitPoint;
            }
            else if(playerMovement.facing == PlayerMovement.Facing.left)
            {
                hitOrigin.localPosition = originalOriginPos - leftHitPoint;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(hitOrigin.position, new Vector2(boxSize, boxSize));
    }

    IEnumerator waitForAttack()
    {
        canAttack = false;
        yield return new WaitForSeconds(attackCoolDownTime);
        canAttack = true;
    }
}
