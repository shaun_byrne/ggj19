﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : Interactable
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnClose()
    {
        base.OnClose();
    }

    public override void Interact()
    {
        base.Interact();
		PlayerHandler ph = FindObjectOfType<PlayerHandler>();
		GameObject player = ph.gameObject;
        if(ph.hasArtifact == false && transform.parent == null)
        {
			ph.artifactShowSprite.SetActive(true);
			ph.CarryArtifact(gameObject);
        }
    }
}
