using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {
	public float moveSpeed = 50;
    public GameObject artifactShowSprite;

	[System.NonSerialized]
	public GameObject heldItem;

	private Pathfinder pathfinder;
	private Collider2D collider;
	private Rigidbody2D rigidbody;
	private GameObject target;
	private GameObject[] artifacts;
	private GameObject waypoint;
	private GameObject spawnPoint;
	private bool returnHome;

	void Start() {
		pathfinder = GameManager.current.pathfinder;
		collider = GetComponent<Collider2D>();
		rigidbody = GetComponent<Rigidbody2D>();
		target = GameManager.current.NearestArtifact(transform.position);
		spawnPoint = GameManager.current.NearestSpawnPoint(transform.position);
	}

	void Update() {
		if (target == null) { return; }

		if (Vector3.Distance(transform.position, target.transform.position) < GameManager.current.characterRadius) {
			if (heldItem != null) {
				DropItem();
				return;
			} else if (returnHome) {
				target = null;
				returnHome = false;
				DestroyImmediate(gameObject);
				GameManager.current.UpdateEnemies();
				return;
			} else {
				PickupItem(target);
			}
		}

		bool hasLOS = HasLOS();
		Vector2 moveDir = Vector2.zero;

		if (hasLOS) {
			waypoint = null;

			if (target != null) {
				moveDir = TargetDir(target);
			}
		} else {
			FindWaypoint();

			if (waypoint != null) {
				moveDir = TargetDir(waypoint);
			}
		}

		rigidbody.AddForce(moveDir * moveSpeed * Time.deltaTime);
	}

	private void FindWaypoint() {
		if (waypoint != null) {
			int layermask = 1 << 12;
			Vector2 rayDir = (Vector2)waypoint.transform.position - (Vector2)transform.position;
			RaycastHit2D hit = Physics2D.CircleCast(transform.position, GameManager.current.characterRadius, rayDir, rayDir.magnitude, layermask);

			if (hit.collider != null) {
				waypoint = pathfinder.NearestWaypointToTarget(transform.position, target.transform.position);
			} else if (Vector3.Distance(transform.position, waypoint.transform.position) < GameManager.current.characterRadius) {
				waypoint = pathfinder.NextWaypoint(waypoint, pathfinder.NearestWaypoint(target.transform.position));
			}
		} else {
			waypoint = pathfinder.NearestWaypointToTarget(transform.position, target.transform.position);
		}
	}

	private Vector2 TargetDir(GameObject t) {
		Vector2 moveDir = t.transform.position - transform.position;
		moveDir.Normalize();
		return moveDir;
	}

	private bool HasLOS() {
		int layermask = 1 << 12;
		Vector2 hitDir = (Vector2)target.transform.position - (Vector2)transform.position;
		float hitDist = hitDir.magnitude;
		RaycastHit2D hit = Physics2D.CircleCast(transform.position, GameManager.current.characterRadius, hitDir, hitDist, layermask);
		return hit.collider == null;//&& hit.collider.tag == "Artifact";
	}

	private void PickupItem(GameObject item) {
		item.SendMessageUpwards("DropItem", SendMessageOptions.DontRequireReceiver);

        item.GetComponentInChildren<SpriteRenderer>().enabled = false;
        if (item.GetComponentInParent<ArtifactHolder>() != null)
        {
            item.GetComponentInParent<ArtifactHolder>().isHolding = false;
            FindObjectOfType<HomeHandler>().artifactCount -= 1;
        }
        artifactShowSprite.SetActive(true);
        heldItem = item;
        heldItem.transform.parent = transform;
        heldItem.transform.localPosition = Vector2.zero;
        target = GameManager.current.RandomConcealment(spawnPoint.transform.position);
       
	}

	public void DropItem() {
		if (heldItem != null) {
            heldItem.GetComponentInChildren<SpriteRenderer>().enabled = true;
            heldItem.transform.parent = null;
			heldItem = null;
            artifactShowSprite.SetActive(false);
        }
		
		target = spawnPoint;
		returnHome = true;
	}
}
