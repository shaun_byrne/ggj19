﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {
	PathNode[] pathNodes;

	public PathNode[] GetPathNodes() {
		return pathNodes;
	}

	public void SetPathNodes(PathNode[] pathNodes) {
		this.pathNodes = pathNodes;
	}
}
