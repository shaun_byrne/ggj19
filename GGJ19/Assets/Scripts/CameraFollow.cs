﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float smoothness;
    public GameObject targetToFollow;
    public float minX, minY, maxX, maxY;
    // Start is called before the first frame update
    void Start()
    {
        
        if(targetToFollow == null)
        {
            targetToFollow = FindObjectOfType<PlayerMovement>().gameObject;
        }
        StartCoroutine(viewHomeAndPlayer());
    }

    void OnlyMoveBetweenX(float minX, float maxX)
    {
        if (transform.position.x >= maxX)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }

        if (transform.position.x < minX)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
    }
    void OnlyMoveBetweenY(float minY, float maxY)
    {
        if (transform.position.y >= maxY)
        {
            transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        }

        if (transform.position.y < minY)
        {
            transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (targetToFollow != null)
        {
            FollowTarget();
        }
        OnlyMoveBetweenX(minX, maxX);
        OnlyMoveBetweenY(minY, maxY);
    }

    void FollowTarget()
    {
        transform.position =
            Vector2.Lerp(transform.position, targetToFollow.transform.position, Time.deltaTime * smoothness);
    }

    IEnumerator viewHomeAndPlayer()
    {
        targetToFollow = FindObjectOfType<HomeHandler>().gameObject;
        yield return new WaitForSeconds(4);
        targetToFollow = FindObjectOfType<PlayerHandler>().gameObject;

    }
}
