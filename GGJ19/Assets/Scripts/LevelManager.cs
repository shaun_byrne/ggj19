﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject levelFader;
    Animator faderAnimator;

    public bool gameOver = false;
    public bool levelFinish = false;
    public Text levelCompleteText;
    public Text gameFinishText;
    public Text gameRuleText;
    public Image icon;
    public int levelIndex = 0;
    public int maxLevelCount;

    public float timeUntilDawn = 200;
    private float DawnTimer = 200; 

    public Transform playerStartPoint;
    public Slider dayNightBar;

    HomeHandler home;
    public GameObject artifactHolder;

    public GameObject level2Pos1;
    public GameObject level2Pos2;

    public GameObject level3Pos1;
    public GameObject level3Pos2;
    GameObject[] enemySpawnPoints;

    public Transform[] spawnPoints;
    public GameObject startingSpawn;
    public GameObject startingArtifact;

    // Start is called before the first frame update
    void Start()
    {
        DawnTimer = timeUntilDawn;
        dayNightBar.value = 0;
        home = FindObjectOfType<HomeHandler>();
        faderAnimator = levelFader.GetComponent<Animator>();

        //ResetPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        gameRuleText.text = 
            home.artifactCount  + "/"  + home.maxArtifactCount + " momentos in place";

        icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, (home.artifactCount * 100 / home.maxArtifactCount *100) * .1f);

        dayNightBar.value = 1f - (DawnTimer / timeUntilDawn);
        if(gameOver == true && Input.GetKeyDown(KeyCode.R))
        {
            ReloadGame();
        }
        else
        {
            DawnTimer -= Time.deltaTime;
        }
        if (DawnTimer <= 0)
        {
            if (home.artifactCount >= home.maxArtifactCount / 2)
            {
                LevelComplete();
            }
            else
            {
                levelLost();
            }
        }
        else
        {
            levelFinish = false;
        }
    }

    public void LevelComplete()
    {
        levelFinish = true;
        if (Input.GetKeyDown(KeyCode.R) && levelFinish == true)
        {
            levelFinish = false;
            faderAnimator.SetTrigger("fadeout");
            faderAnimator.SetTrigger("fadein");
            StartCoroutine(ResetAfterAWhile());
        }
        else
        {
            levelCompleteText.text = "Rise the sun, and with it, the end of the cruel menace.\n The memory hearth remains whole...\n 'r' to rest";
            foreach (Enemy enemy in FindObjectsOfType<Enemy>())
            {
                Destroy(enemy.gameObject);
            }

        }
        if (levelIndex >= maxLevelCount - 1)
        {
            faderAnimator.SetBool("Finish", true);
            gameOver = true;
            levelCompleteText.text = "";
            gameFinishText.text = "You";
            GameManager.current.stopSpawn = true;
            GameManager.current.audioManager.PlayAudio("gameOver");
        }
        
        foreach(TorchHandler torch in FindObjectsOfType<TorchHandler>())
        {
            Destroy(torch.gameObject);
        }
    }

    public void levelLost()
    {
        levelCompleteText.text = "The cruel sun rises upon the broken fragments of the memory hearth...\n Only sorrow and loss remain.\n 'r' to restart";
        gameOver = true;
        GameManager.current.audioManager.PlayAudio("gameOver");
    }

    public void ReloadGame()
    { 
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ResetGameLevelWhenWin()
    {
        levelCompleteText.text = "";
        FindObjectOfType<HomeHandler>().maxArtifactCount += 2;
        levelIndex += 1;
        FindObjectOfType<PlayerHandler>().gameObject.transform.position = (Vector2)spawnPoints[Random.Range(0, 8)].position + (Vector2)Random.onUnitSphere.normalized * 2f;
        ResetPlayer();
        FindObjectOfType<HomeHandler>().AddArtifacts();
        FindObjectOfType<HomeHandler>().artifactCount = FindObjectOfType<HomeHandler>().maxArtifactCount;
        GameManager.current.NewLevel(FindObjectOfType<HomeHandler>().maxArtifactCount);

        if (levelIndex == 1)
        {
            Instantiate(artifactHolder, level2Pos1.transform.position, Quaternion.identity);
            Instantiate(artifactHolder, level2Pos2.transform.position, Quaternion.identity);
        }
        else if(levelIndex == 2)
        {
            Instantiate(artifactHolder, level3Pos1.transform.position, Quaternion.identity);
            Instantiate(artifactHolder, level3Pos2.transform.position, Quaternion.identity);
        }
    }

    IEnumerator ResetAfterAWhile()
    {
        DawnTimer = timeUntilDawn;
        yield return new WaitForSeconds(2);
        ResetGameLevelWhenWin();
    }

    public void ResetPlayer()
    {
        GameObject player = FindObjectOfType<PlayerHandler>().gameObject;
        player.gameObject.transform.position = (Vector2)startingSpawn.transform.position;
        //startingArtifact.transform.position = (Vector2)player.transform.position - (Vector2)Vector2.up * 2f;
    }
}
