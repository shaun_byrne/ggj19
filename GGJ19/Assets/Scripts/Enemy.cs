using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject trailFX;
    public GameObject hitFX;
    public GameObject bloodFX;
    public float trailSpawnRate;
    public bool hasStop;
    SpriteRenderer renderer;
	Rigidbody2D rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponentInChildren<SpriteRenderer>();
		rigidbody = GetComponent<Rigidbody2D>();
		
        if (trailFX != null)
        {
            StartCoroutine(SpawnTrail());
        }
    }

    // Update is called once per frame
    void Update()
    {
		if (rigidbody.velocity.x > 0) {
			renderer.transform.localScale = new Vector3(-Mathf.Abs(renderer.transform.localScale.x),
				renderer.transform.localScale.y,
				renderer.transform.localScale.z);
		} else if (rigidbody.velocity.x < 0) {
			renderer.transform.localScale = new Vector3(Mathf.Abs(renderer.transform.localScale.x),
				renderer.transform.localScale.y,
				renderer.transform.localScale.z);
		}
    }

    public void TakeDamage()
    {
        GameManager.current.audioManager.PlayAudio("hit");
        Time.timeScale = Random.Range(.24f, .026f);
        Instantiate(hitFX, transform.position, Quaternion.identity);
        Instantiate(bloodFX, transform.position, Quaternion.identity);
        DropItem();
        FindObjectOfType<ShakeController>().CamBigShake();
        print("Damaged");
        StartCoroutine(DamageEffect());
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void DropItem()
    {
        GetComponent<EnemyAI>().DropItem();
    }

    IEnumerator DamageEffect()
    {
        renderer.color = Color.red;
        yield return new WaitForSeconds(.3f);
        renderer.color = Color.white;
    }

    IEnumerator SpawnTrail()
    {
        while (true)
        {
            if (GetComponent<EnemyAI>().heldItem != null) {
                Instantiate(trailFX, transform.position, Quaternion.identity);
            }

            yield return new WaitForSeconds(trailSpawnRate);
        }
    }
}
