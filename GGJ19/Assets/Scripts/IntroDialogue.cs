﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroDialogue : MonoBehaviour {
	public Text[] text;
	public float readSpeed = 0.1f;
	public float breakTime = 1.0f;
	public float holdTime = 1.0f;

	private string currentText;
	private int index = 0;
	private int charIndex;
	private float readTimer = 0f;
	private float breakTimer = 0f;
	private bool didBreak = false;

	void Start() {
		foreach (Text t in text) {
			t.gameObject.SetActive(false);
		}
	}

	void Update() {
		if (index < text.Length) {
			if (!text[index].gameObject.activeInHierarchy) {
				charIndex = 0;
				didBreak = false;
				readTimer = breakTime;
				currentText = text[index].text;
				text[index].text = "";
				text[index].gameObject.SetActive(true);
			}

			if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape)) {
				text[index].text = currentText;
				charIndex = currentText.Length;
			}

			if (readTimer <= 0f) {
				if (breakTimer <= 0f) {
					readTimer = readSpeed;

					if (charIndex < currentText.Length) {
						text[index].text += currentText[charIndex++];
					} else if (!didBreak) {
						didBreak = true;
						breakTimer = breakTime;
					} else {
						text[index].gameObject.SetActive(false);
						index++;
					}
				} else {
					breakTimer -= Time.deltaTime;
				}
			} else {
				readTimer -= Time.deltaTime;
			}
		} else {
			print(Time.realtimeSinceStartup);
			SceneManager.LoadScene(1);
		}
	}
}
