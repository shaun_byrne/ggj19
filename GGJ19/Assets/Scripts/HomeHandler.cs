﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeHandler : Interactable
{
    public int artifactCount = 6;
    public int maxArtifactCount = 6;
    public GameObject[] artifacts;
    public GameObject spawnArtFX;
    public GameObject FireRetriveFX;
    GameObject player;
	int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnArtAtTime());
        player = FindObjectOfType<PlayerHandler>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(player.transform.position.y < transform.position.y)
        {
            GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
        }
    }

    void SpawnArtifacts()
    {
        GameObject art = Instantiate(artifacts[index++],
               (Vector2)transform.position + new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f)),
               Quaternion.identity);
        art.transform.SetParent(gameObject.transform);
        GameManager.current.UpdateArtifacts();
        Instantiate(spawnArtFX, art.transform.position, Quaternion.identity);

		if (index >= artifacts.Length) {
			index = 0;
		}
    }

    void SpawnArtAtHolder()
    {
        ArtifactHolder[] holders = FindObjectsOfType<ArtifactHolder>();
        foreach (ArtifactHolder holder in holders)
        {
            if (holder.isHolding == false)
            {
                holder.isHolding = true;
                GameObject art = Instantiate(artifacts[index++],
                       holder.gameObject.transform.position,
                       Quaternion.identity);
                art.transform.SetParent(holder.transform);
                GameManager.current.UpdateArtifacts();
                Instantiate(spawnArtFX, art.transform.position, Quaternion.identity);

				if (index >= artifacts.Length) {
					index = 0;
				}

				return;
            }
        }
    }

    public void AddArtifacts()
    {
        StartCoroutine(AddSpawnArtAtTime());
    }

    public override void Interact()
    {
        base.Interact();
        ArtifactHolder[] holders = FindObjectsOfType<ArtifactHolder>();
        PlayerHandler player = FindObjectOfType<PlayerHandler>();
        if(player.hasArtifact == true && artifactCount < maxArtifactCount)
        {
            GameManager.current.audioManager.PlayAudio("artifactDrop");
            artifactCount += 1;
            Instantiate(FireRetriveFX);
            if (player.GetComponentInChildren<Item>() != null)
            {
                foreach(ArtifactHolder holder in holders)
                {
                    print(holder.name);
                    if (holder.isHolding == false)
                    {
						player.DropItem();
                        holder.isHolding = true;
                        player.GetComponentInChildren<Item>().gameObject.transform.position = 
                            holder.gameObject.transform.position;
                        player.GetComponentInChildren<Item>().gameObject.transform.SetParent(holder.gameObject.transform);
                        FindObjectOfType<ShakeController>().CamShake();
                        return;
                    }
                }
            }
            //Play animation
        }
    }

    IEnumerator SpawnArtAtTime()
    {
        int i = 0;
        while(i < maxArtifactCount - 1)
        {
            //SpawnArtifacts();
            SpawnArtAtHolder();
            yield return new WaitForSeconds(.8f);
            i++; 
        }
    }

    IEnumerator AddSpawnArtAtTime()
    {
        int i = artifactCount;
        while (i <= maxArtifactCount)
        {
            //SpawnArtifacts();
            SpawnArtAtHolder();
            yield return new WaitForSeconds(.5f);
            i++;
        }
    }
}
