﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandler : MonoBehaviour
{
    int torchNum = 1;
    public GameObject torch;
    public bool canPlaceTorch = false;
    public Vector2 spawnPosition;
    public bool hasArtifact = false;
	public GameObject artifactShowSprite;

	[System.NonSerialized]
	public GameObject heldItem;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.K) && canPlaceTorch == true)
        {
            PlaceTorch(spawnPosition);
            FindObjectOfType<ShakeController>().CamShake();
        }
        HandleInteractable();
    }

    public void PlaceTorch(Vector2 spawnPosition)
    {
        if(torchNum >= 1)
        {
            Instantiate(torch, spawnPosition, Quaternion.identity);
            torchNum -= 1;
        }
    }

    public void CarryArtifact(GameObject item)
    {
		heldItem = item;
		artifactShowSprite.SetActive(true);
		heldItem.transform.parent = transform;
		heldItem.transform.localPosition = Vector2.zero;
		hasArtifact = true;

		if (heldItem) {
			heldItem.GetComponentInChildren<SpriteRenderer>().enabled = false;
		}
    }

	public void DropItem() {
		hasArtifact = false;
		artifactShowSprite.SetActive(false);

		if (heldItem) {
			heldItem.GetComponentInChildren<SpriteRenderer>().enabled = true;
		}
	}

    void HandleInteractable()
    {
        Collider2D[] interactables = Physics2D.OverlapCircleAll(transform.position, 1);
        foreach (Collider2D collider in interactables)
        {
            Interactable interactable = collider.GetComponent<Interactable>();
            if (interactable != null)
            {
                interactable.OnClose();
                if (Input.GetKeyDown(KeyCode.F) || Input.GetButtonDown("Fire3"))
                {
                    interactable.Interact();
                }
            }
        }
    }
}
